package masterSpringMvc.authentication;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Justyna on 2017-02-28.
 */
@Controller
public class LoginController {
    @RequestMapping("/login")
    public String authenticate(){
        return "login";
    }
}
