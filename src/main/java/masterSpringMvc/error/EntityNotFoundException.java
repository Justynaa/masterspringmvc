package masterSpringMvc.error;

/**
 * Created by Justyna on 2017-02-27.
 */
public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(String message){
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause){
        super(message, cause);
    }
}
